package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejemplo02GradleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo02GradleAppApplication.class, args);
	}

}
