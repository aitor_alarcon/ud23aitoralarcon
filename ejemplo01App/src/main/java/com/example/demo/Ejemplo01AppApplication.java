package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejemplo01AppApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo01AppApplication.class, args);
	}

}
